(function($) { 
"use strict"; 

    $( window ).resize(function() {

        var width = $(window).width();

        if( $('.momoko-creative-slider').hasClass('creative-v2-active')) {

            if( width <= 768 ) {

                $('.momoko-creative-slider').addClass('creative_slider_v1');
                $('.momoko-creative-slider').removeClass('creative_slider_v2');

            } else if( width > 768 ) {

                $('.momoko-creative-slider').addClass('creative_slider_v2');
                $('.momoko-creative-slider').removeClass('creative_slider_v1');
                
            }

        }

    });

    jQuery(document).ready(function ($) {

    if( $('div').hasClass('momoko-creative-slider') ) {

        $('.momoko-creative-slider').momokoSlider();

        if( $('.momoko-item').hasClass('hover')) {

            $('.momoko-item.active').css({'opacity':'0'});
        }

    }

    if( $(".container > div > ul").hasClass("momoko-masonry") ) {
        
        //Masonry
        var container = document.querySelector('#momoko-masonry');
        var msnry;
        imagesLoaded( container, function() {
            msnry = new Masonry( container, {
                itemSelector: '.container > div > ul > li',
                gutter: 60
            });
        });
    }

    // Sticky Menu
    var stickyNavTop = $('.momoko-main-navigation').offset().top;
     
    var stickyNav = function(){
        var scrollTop = $(window).scrollTop();

        if (scrollTop > 350) { 
            $('.hide-header-image').addClass('momoko-sticky');
        } else {
            $('.hide-header-image').removeClass('momoko-sticky');
        }

        if (scrollTop > 620) { 
            $('.active-header-image').addClass('momoko-sticky');
        } else {
            $('.active-header-image').removeClass('momoko-sticky');
        }

    };

    stickyNav();
     
    $(window).scroll(function() {
      stickyNav();
    });

    //Featured slider

    $('.momoko-classic-slider').slick({
        "fade": false,
        "dots": false,
        "nextArrow": '<span class="arrow-next"></span>',
        "prevArrow": '<span class="arrow-prev"></span>',
        "infinite": true,
        "arrows": true,
        "autoplay": true,
        "autoplaySpeed": 3000,
        "centerMode": true,
        "slidesToShow": 3,
        "responsive": [{
          "breakpoint": 1200,
          "settings": {
            "slidesToShow": 2,
            "centerMode": false,
          }
        }],
    });

    $('.default-slide').slick({
        "dots": true,
        "infinite": true,
        "nextArrow": '<span class="arrow-next"></span>',
        "prevArrow": '<span class="arrow-prev"></span>', 
        "autoplay": false,
        "fade": true,
        "cssEase": 'linear',
        "autoplaySpeed": 3000
    });

    $('.one-slide').slick({
        "dots": true,
        "infinite": true,
        "arrows": false,
        "nextArrow": '<span class="arrow-next"></span>',
        "prevArrow": '<span class="arrow-prev"></span>', 
        "autoplay": true,
        "fade": true,
        "cssEase": 'linear',
        "autoplaySpeed": 3000
    });

    $('.two-slide').slick({
        "dots": false,
        "infinite": true,
        "nextArrow": '<span class="arrow-next"></span>',
        "prevArrow": '<span class="arrow-prev"></span>', 
        "autoplay": true,
        "autoplaySpeed": 3000,
        "slidesToShow": 2,
        "centerMode": true,
        "centerPadding": '',
    });

    $('.three-slide').slick({
        "dots": false,
        "nextArrow": '<span class="arrow-next"></span>',
        "prevArrow": '<span class="arrow-prev"></span>',
        "infinite": true,
        "arrows": true,
        "autoplay": true,
        "autoplaySpeed": 3000,
        "centerMode": false,
        "slidesToShow": 4,
        "responsive": [{
          "breakpoint": 1200,
          "settings": {
            "slidesToShow": 2,
            "centerMode": false,
          }
        }]
    });

    //Slider Option for Gallery Post
    $('.post-gallery').addClass('owl-carousel');
    
    if ($('.post-gallery.owl-carousel').length) {
    $('.post-gallery.owl-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 1000,
        autoplayHoverPause: true,
        nav: true,
        navText: ["<span class='momoko-prev'></span>","<span class='momoko-next'></span>"],
        dots: false,
        loop: true,
        });
    }

    //active section 

    $('.active-section-slider').addClass('owl-carousel');
    
    if ($('.active-section-slider.owl-carousel').length) {

        $('.active-section-slider.owl-carousel').owlCarousel({
            items: 3,
            autoplay: true,
            smartSpeed: 1500,
            margin: 40,
            autoplayHoverPause: true,
            nav: false,
            navText: ["<span class='momoko-prev'></span>","<span class='momoko-next'></span>"],
            dots: false,
            loop: true,
            responsive:{
                0:{
                    margin: 0,
                    items:1,
                    nav: true,
                },
                600:{
                    margin: 0,
                    items:1,
                    nav: true,
                },
                1000:{
                    items:3,
                }
            } 
        });
    }

    

    //Slider Option for instagram in win sidebar
     $('.momoko-win-sidebar .instagram-size-original').addClass('owl-carousel');

    if ($('.momoko-win-sidebar .instagram-size-original.owl-carousel').length) {
    $('.momoko-win-sidebar .instagram-size-original.owl-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 1000,
        autoplayHoverPause: true,
        nav: true,
        navText: ["<span class='momoko-prev'></span>","<span class='momoko-next'></span>"],
        dots: false,
        loop: true
        });
    }

    //Slider Option for instagram in single sidebar
    $('.momoko-sidebar-area .instagram-size-original').addClass('owl-carousel');

    if ($('.momoko-sidebar-area .instagram-size-original.owl-carousel').length) {
    $('.momoko-sidebar-area .instagram-size-original.owl-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 1000,
        autoplayHoverPause: true,
        nav: true,
        navText: ["<span class='momoko-prev'></span>","<span class='momoko-next'></span>"],
        dots: false,
        loop: true
        });
    }

    //Slider Option for instagram in sidebar
     $('.momoko-page-sidebar .instagram-size-original').addClass('owl-carousel');

    if ($('.momoko-page-sidebar .instagram-size-original.owl-carousel').length) {
    $('.momoko-page-sidebar .instagram-size-original.owl-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 1000,
        autoplayHoverPause: true,
        nav: true,
        navText: ["<span class='momoko-prev'></span>","<span class='momoko-next'></span>"],
        dots: false,
        loop: true
        });
    }

    //Instagram slider
    $('.momoko-footer-sidebar-area .instagram-size-original').addClass('owl-carousel');

    if ($('.momoko-footer-sidebar-area .instagram-size-original.owl-carousel').length) {
        $('.momoko-footer-sidebar-area .instagram-size-original.owl-carousel').owlCarousel({
            items: 1,
            autoplay: true,
            autoplayHoverPause: true,
            nav: true,
            navText: ["<span class='momoko-prev'></span>","<span class='momoko-next'></span>"],
            smartSpeed: 1000,
            margin: 30,
            dots: false,
            loop: true
        });
    }

    //Recent posts slider
    $('.rpwe-ul').addClass('owl-carousel');

    if ($('.rpwe-ul.owl-carousel').length) {
        $('.rpwe-ul.owl-carousel').owlCarousel({
            items: 1,
            autoplay: true,
            autoplayHoverPause: true,
            nav: false,
            navText: ["<span class='momoko-prev'></span>","<span class='momoko-next'></span>"],
            smartSpeed: 1200,
            margin: 30,
            dots: true,
            loop: true
        });
    } 
    
    
    //menu
    $('.momoko-main-navigation .menu-item-has-children').prepend('<span class="menu-icon"></span>');
    $('.momoko-main-navigation .menu .menu-icon').click( function() {
        var $submenu = $(this).closest('.menu-item-has-children').find(' > .sub-menu');
        $submenu.slideToggle(500);
    return false;
    });

    /*instagram*/
    var $instagram_items = $('.momoko-instagram-feed .instagram-pics.instagram-size-original li');
    if ( $instagram_items.length ) {
        var $item_width = parseFloat( 100 / $instagram_items.length ).toFixed(5);
        $instagram_items.css({
            'width': $item_width + '%'
        })
    }

    //open desctop search
    $('.nav-search-icon').on('click',null,function() {
        $('body').toggleClass('search-active');
    });

    //open mobile search
    $('.mobile-search-icon').on('click',null,function() {
        $('body').toggleClass('search-active');
    });

    //win sidebar
    $('.site-sidebar').on('click',null,function() {
        $('.momoko-win-sidebar').toggleClass('active');
        $('body').toggleClass('active-sidebar');
        $('.back-top').toggleClass('active');
    });

    //nav button
    $('.momoko-nav-button').on('click',null,function() {
        $('.menu').toggleClass('active');
        $('body').toggleClass('active');
    });

    $('.momoko-background').on('click',null,function() {
        $('body.active').removeClass('active');
        $('body.search-active').removeClass('search-active');
        $('body.active-sidebar').removeClass('active-sidebar');
        $('.momoko-win-sidebar.active').removeClass('active');
        $('.menu.active').removeClass('active');
        $('.back-top').removeClass('active');
    });

    $('.site-sidebar').on('click',null,function() {
        $('.menu.active').removeClass('active');
        $('body.active').removeClass('active');
    });

    $('.momoko-nav-button').on('click',null,function() {
        $('.momoko-win-sidebar.active').removeClass('active');
        $('body.active-sidebar').removeClass('active-sidebar');
    });

        //close nav search
        $('.site-content').on('click',null,function() {
            $('.nav-search').removeClass('active');
        });

        $('.menu-icon').on('click',null,function() {
            $(this).toggleClass('active');
        });
            
        $(window).scroll(function () {

            var width           = $(window).width();
            var nav             = $('.admin-bar .momoko-main-navigation');
            var nav_menu        = $('.admin-bar .momoko-main-navigation .menu');
            var nav_search      = $('.admin-bar .nav-search-icon');
            var mobile_search   = $('.admin-bar .mobile-search-icon');
            var site_sidebar    = $('.admin-bar .momoko-win-sidebar');
            var mobile_social   = $('.admin-bar .momoko-nav .momoko-nav-social-area');
           
                
            if ( $(this).scrollTop() === 0 && width <= 600 ) {
               nav.css({'top': '46px'});
               mobile_social.css({'top': '45px'});
               nav_menu.css({'top': '102px'});
               site_sidebar.css({'top': '102px'});
               nav_search.css({'top': '46px'});
               mobile_search.css({'top': '46px'});
            } else if ( $(this).scrollTop() >= 0 && width <= 600 ) {
                nav.css({'top': '0px'});
                mobile_social.css({'top': '0px'});
                nav_menu.css({'top': '55px'});
                site_sidebar.css({'top': '55px'});
                nav_search.css({'top': '0px'});
                mobile_search.css({'top': '0px'});
            }

        });

        // Search settings

        $('input[name="post_password"]').attr('placeholder', 'Enter Password ...');
        $('input[name="user_login"]').attr('placeholder', 'Username or email');
        $('input[name="s"]').attr('placeholder', 'Enter Keywords ...');
        //fitvids
        $(".post-video").fitVids();

        $(function (){

            $("#back-top").hide();

            $(window).scroll(function (){
                if ($(this).scrollTop() >= 700){
                    $("#back-top").fadeIn();
                } else{
                    $("#back-top").fadeOut();
                }
            });

            $("#back-top").on('click',null,function() {
                $("body,html").animate({
                    scrollTop:0
                }, 800);
                return false;
            });
        });

        $(function () {
            var myElement = $('.momoko-nav-logo');
            $(window).on('scroll', function () {
                var st = $(this).scrollTop();
                myElement.css({
                    'opacity': (st / 600)
                });
            });
        });
    });
})(jQuery);