(function ($) {
	
	"use strict";

    $.fn.momokoSlider = function( options ) {

    	options = options || {};

    	var _this = $(this);

    	var settings = $.extend({
            currentSlide: 0,
            time: 180,
            autoplay: true,
        }, options );

        var momokoSliderItems = [];
    	_this.children('.momoko-item').each(function () {
        	momokoSliderItems.push($(this));
    	});

    	var currentFrame = 0;
    	var momokoSliderState = "play";
    	var length = momokoSliderItems.length - 1;
    	var time = 0;

    	function frame() {

	        if (momokoSliderState === "play") {

	            currentFrame++;

	            if( currentFrame >= time ) {

	                time = settings.time;


	                _this.find('.momoko-item').removeClass('active');
	                momokoSliderItems[settings.currentSlide].addClass('active');
		       		$('#momoko_number').text(settings.currentSlide+1);

	                if( settings.currentSlide < length ) {

	                    settings.currentSlide++;
	                
	                } else {
	                	
	                    settings.currentSlide = 0;
	                
	                }

	                currentFrame = 0;

	            }

	        }

	        requestAnimationFrame(frame);

	    }

	    if (settings.autoplay) {
	    	frame();
	    }

	    $.fn.play = function () {
	    	momokoSliderState = "play";
	    	frame();
	    }

	    $.fn.stop = function () {
	    	momokoSliderState = "paused";
	    }

	    var setOpacity = function (element, opacity) {
	    	$(element).css({'opacity':opacity});
	    }

	    _this.children('.momoko-item').hover(function (e) {

	        $(this).toggleClass('hover');

	        if (e.type === "mouseenter") {

	            momokoSliderState = "paused";

	        	setOpacity(_this.find('.momoko-creative-item'), '0');
	        	setOpacity($(this).find('.momoko-creative-item'), '');

	        } else {

	            momokoSliderState = "play";

	        	setOpacity(_this.find('.momoko-creative-item'), '');

	        }

	    });

    	return _this;

	};
	 
})(jQuery);